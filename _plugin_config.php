<?php
return [
    'SelifaVersion' => '1.1.1',
    'SelifaName' => 'SELIFA',
    'CoreInclude' => 'libraries/RBS/Selifa/Core.php',
    'InitContent' => '_init_content.txt'
];
?>