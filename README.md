# Selifa Core #

### Introduction ###
**Selifa Core** is a basic PHP framework for any application type. It offers only a PSR-4 class autoloader, a configuration management and an exception management.
It's configuration differs from most other PHP framework since it only load specific configuration file or spesific class.
This version of Selifa is a major rewrite of my previous Selifa Framework which contains a full framework.

Version 1.0

### Usage Instructions ###
Download or pull this repository into your project. Selifa Core only have a few files so it won't make your project bigger.
For a simple project, you can specify `SELIFA_ROOT_PATH` constant and include `selifa.php` file in folder `selifa`. That's all. Now your project have capability to use PSR-4 autoloader. Just put all of your classes into `libraries` folder.
```php
define('SELIFA_ROOT_PATH',dirname(__FILE__));
include('selifa/selifa.php`);
```

Advanced usage requires you to create your own selifa initialization. Don't worry, it's not that hard.

1. First, you need to define some constants. Put these constants before everything else.
```php
define('SELIFA_TIME_STARTED',microtime(true));
define('SELIFA','v1.0');
define('SELIFA_NAME','Selifa');
```
2. Then you need to define where is selifa location in your filesystem. Create `SELIFA_ROOT_PATH` constants pointing at your project's base directory.  
Usually we use `dirname` to automate the value of this constant, but you can manually specify the exact path.  
Note: full pathname is required.
```php
define('SELIFA_ROOT_PATH',dirname(__FILE__));
// or
define('SELIFA_ROOT_PATH','/var/www/yourproject');
```
3. Then include Selifa's core class file. You can use `SELIFA_ROOT_PATH` or manually specify the file.
```php
include(SELIFA_ROOT_PATH.'/selifa/core/Core.php');
// or
include(SELIFA_ROOT_PATH.'/var/www/yourproject/selifa/core/Core.php');
```
4. Next, initialize the Selifa itself. Use `SELIFA_ROOT_PATH` as `RootPath` value, and specify any folder within `RootPath` for configuration files.  
For single application type (this is for most of the case):
```php
\RBS\Selifa\Core::Initialize(array(
    'RootPath' => SELIFA_ROOT_PATH,
    'ConfigDir' => 'configs/',
    'LoadComponents' => array(
        '\RBS\Selifa\XM' //this must be preloaded at the beginning.
    )
));
```
For multi application type:
```
\RBS\Selifa\Core::Initialize(array(
    'RootPath' => SELIFA_ROOT_PATH,
    'ConfigDir' => 'apps/yourapplicationid/configs/',
    'LoadComponents' => array(
        '\RBS\Selifa\XM'
    )
));
```  
You can specify more than one class that need to be loaded at the beginning. By default, Selifa Core only load class file as soon as they are required somewhere within your application. Putting this class inside `LoadComponents` array will preload it whether you use it or not.  

5. Finish! you can start write your code.

### Credits ###
Rinardi B. Sarean, rinardi_1518_sarean@hotmail.com.

## License ###
The MIT License (MIT)

Copyright (c) 2015-2017. Rinardi B. Sarean

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.