<?php
define('SELIFA_ROOT_PATH',dirname(__FILE__));
define('SELIFA_TIME_STARTED',microtime(true));
define('SELIFA','v1.0');
define('SELIFA_NAME','Selifa');
include(SELIFA_ROOT_PATH.'/libraries/RBS/Selifa/Core.php');

/**
 * Dump $var to error log.
 *
 * NB: Obsolete! Use XM::DumpToErrorLog instead.
 * Will be removed in future release.
 *
 * @param $var
 */
function dumpToErrorLog($var)
{
    ob_start();
    var_dump($var);
    $s = ob_get_contents();
    ob_end_clean();
    error_log('DUMP: '.$s,0);
}

/*
 *  docker run --rm --interactive --tty --volume $PWD/src:/app composer create-project selifa/core=dev-stable . --prefer-dist --ignore-platform-reqs
 *
    Initialization example
    Each application need it own Core::Initialize part.
    For example if you put your applications if path apps/web,
        1. Create file named `index.php` in your application path.
        2. Put these lines into `index.php`
        3. You can put another initialization or package in this file later.

include('../../init.php');

use RBS\Selifa\Core;
Core::Initialize(array(
    'RootPath' => SELIFA_ROOT_PATH,
    'SelifaPath' => 'selifa', //Set selifa core default directory. Leave it to default or remove it from initialization for default configuration.
    'DefaultConfigDir' => 'selifa/defaults', //Set default configuration directory. Useful for multiple applications share same configurations.
    'ConfigDir' => 'configs', // Set configuration directory for this specific application.
    'CacheConfiguration' => false, //Set to true to enable configuration cache. Use cache for staging or production only.
    'EnvironmentVars' => LOAD_AND_PUT_ENV, //Set how Configuration class will use .env file found in the root path. Options: IGNORE_ENV, LOAD_ENV, LOAD_AND_PUT_ENV.
    'ComponentPath' => 'components', //Obsolete, remove it on usage. For the sake of completeness only.
    'LibraryPath' => 'libraries' => //Set default libraries directory. Leave it to default or remove it from initialization for default configuration.
    'DriverPath' => 'drivers' => //Set default drivers directory. Leave it to default or remove it from initialization for default configuration.
    'UseComposer' => true, //Set to true to enable composer loader file.
    'ComposerVendorPath' => 'vendor' //Default composer vendor directory. You can change this depends on your composer configuration.
    'LoadComponents' => array(
        // List any component to be loaded during initialization. Make it a short list though.
        '\RBS\Selifa\XM' => array(
             'EnableTrace' => true, //Enable trace output on exception.
             'VerboseInternalException' => false, //Enable verbose internal selifa exception.
             'VerboseSystemException' => false, //Enable verbose system exception.
             'TraceExceptionTree' => true, //Enable show everything on exception tree.
             'HandleDefaultException' => true, //Enable handle every unhandled exception.
             'Transmitters' => array(
                //Register any transmitter class according to your use case.
                 'RBS\Selifa\Exception\ConsoleOutputTransmitter' //Write exception on console output.
             )
        )
    )
));

Note on 'EnvironmentVars':
 - Processing environment vars will take a little bit amount of resource. Please consider this on high traffic application.
 - On docker environment, you should rely on docker environment variables as much as possible to eliminate usage on application env.
 - Set 'EnvironmentVars' to IGNORE_ENV to disable application env processing.
 - Configuration::Env method will get any environment variables defined in the system, including anything defined in docker.
   This method will also get application env if `EnvironmentVars` set to LOAD_AND_PUT_ENV.



Default implementation, Quick and Simple :

include('../../init.php');

use RBS\Selifa\Core;
Core::Initialize(array(
    'RootPath' => SELIFA_ROOT_PATH,
    'ConfigDir' => 'configs',
    'LoadComponents' => array(
        '\RBS\Selifa\XM'
    )
));

Default implementation, Simple and more error options :

include('../../init.php');

use RBS\Selifa\Core;
Core::Initialize(array(
    'RootPath' => SELIFA_ROOT_PATH,
    'ConfigDir' => 'configs',
    'LoadComponents' => array(
        '\RBS\Selifa\XM' => array(
             'EnableTrace' => true,
             'VerboseInternalException' => false,
             'VerboseSystemException' => false,
             'TraceExceptionTree' => true,
             'HandleDefaultException' => true
        )
    )
));
*/
?>