<?php
use PHPUnit\Framework\TestCase;

/**
 * Class ExceptionFeatureTest
 */
class ExceptionFeatureTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $path = (dirname(__FILE__).'/../init.php');
        include($path);

        $coreOpts = [
            'RootPath' => SELIFA_ROOT_PATH,
            'DefaultConfigDir' => 'defaults',
            'ConfigDir' => 'configs',
            'EnvironmentVars' => 3,
            'UseComposer' => true,
            'LoadComponents' => [
                '\RBS\Selifa\XM' => [
                    'EnableTrace' => false,
                    'VerboseInternalException' => false,
                    'VerboseSystemException' => false,
                    'TraceExceptionTree' => false,
                    'HandleDefaultException' => true,
                    'TranslatorClass' => '\RBS\Selifa\Exception\Translator',
                    'Transmitters' => [

                    ]
                ],
                '\RBS\Selifa\Exception\Translator' => [
                    'DefaultLang' => 'en'
                ]
            ]
        ];
        \RBS\Selifa\Core::Initialize($coreOpts);

        $tContent = [
            '410404' => 'Specific item {!name} does not exists.',
            '*404' => '{!key} item {!name} does not exists.',
            '*5708' => '{!key} item does not implement {!name} wrapper.',
            '*35505' => '{!key} item does not use {!name} things.'
        ];

        $sPath = (SELIFA_ROOT_PATH.DIRECTORY_SEPARATOR.'lang'.DIRECTORY_SEPARATOR.'en');
        if (!file_exists($sPath))
            mkdir($sPath,0777,true);

        $s = "<?php\nreturn ".var_export($tContent,true).";\n?>\n";
        file_put_contents($sPath.DIRECTORY_SEPARATOR.'error.php',$s);
    }

    public static function tearDownAfterClass(): void
    {
        $sFile = (SELIFA_ROOT_PATH.DIRECTORY_SEPARATOR.'lang');
        $sFile1 = (SELIFA_ROOT_PATH.DIRECTORY_SEPARATOR.'lang'.DIRECTORY_SEPARATOR.'en');
        unlink($sFile1.DIRECTORY_SEPARATOR.'error.php');
        rmdir($sFile1);
        rmdir($sFile);
    }

    public function testExceptionTranslation_1()
    {
        try
        {
            throw new Exception('EXCEPTION TEST',410404);
        }
        catch (Exception $x)
        {
            $result = \RBS\Selifa\XM::Instance()->Handle($x);
            $this->assertArrayHasKey('M',$result);
            $this->assertSame('Specific item  does not exists.',$result['M']);
        }
    }

    public function testExceptionTranslation_2()
    {
        try
        {
            throw new SelifaException(410404,'',[
                'name' => 'TEST'
            ]);
        }
        catch (Exception $x)
        {
            $result = \RBS\Selifa\XM::Instance()->Handle($x);
            $this->assertArrayHasKey('M',$result);
            $this->assertSame('Specific item TEST does not exists.',$result['M']);
        }
    }

    public function testExceptionTranslation_3()
    {
        try
        {
            throw new SelifaException(780404,'',[
                'key' => 'TEST',
                'name' => 'FOOD'
            ]);
        }
        catch (Exception $x)
        {
            $result = \RBS\Selifa\XM::Instance()->Handle($x);

            $this->assertArrayHasKey('C',$result);
            $this->assertSame(780404,$result['C']);
            $this->assertArrayHasKey('M',$result);
            $this->assertSame('TEST item FOOD does not exists.',$result['M']);
        }
    }

    public function testExceptionTranslation_4()
    {
        try
        {
            throw new SelifaException(675708,'',[
                'key' => 'FACTORY',
                'name' => 'FOOD'
            ]);
        }
        catch (Exception $x)
        {
            $result = \RBS\Selifa\XM::Instance()->Handle($x);

            $this->assertArrayHasKey('C',$result);
            $this->assertSame(675708,$result['C']);
            $this->assertArrayHasKey('M',$result);
            $this->assertSame('FACTORY item does not implement FOOD wrapper.',$result['M']);
        }
    }

    public function testExceptionTranslation_5()
    {
        try
        {
            throw new SelifaException(735505,'',[
                'key' => 'HOMEMADE',
                'name' => 'BAD'
            ]);
        }
        catch (Exception $x)
        {
            $result = \RBS\Selifa\XM::Instance()->Handle($x);

            $this->assertArrayHasKey('C',$result);
            $this->assertSame(735505,$result['C']);
            $this->assertArrayHasKey('M',$result);
            $this->assertSame('HOMEMADE item does not use BAD things.',$result['M']);
        }
    }
}
?>