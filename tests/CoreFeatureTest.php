<?php
use PHPUnit\Framework\TestCase;

/**
 * Class CoreFeatureTest
 */
final class CoreFeatureTest extends TestCase
{
    /**
     * @small
     * @testdox Initializing selifa core
     */
    public function testCoreLoad()
    {
        $path = (dirname(__FILE__).'/../init.php');
        include($path);

        $classes = get_declared_classes();
        $this->assertContains("RBS\\Selifa\\Core",$classes);

        $coreOpts = [
            'RootPath' => SELIFA_ROOT_PATH,
            'DefaultConfigDir' => 'defaults',
            'ConfigDir' => 'configs',
            'EnvironmentVars' => 3,
            'UseComposer' => true,
            'LoadComponents' => [
                '\RBS\Selifa\XM' => [
                    'EnableTrace' => false,
                    'VerboseInternalException' => false,
                    'VerboseSystemException' => false,
                    'TraceExceptionTree' => false,
                    'HandleDefaultException' => true,
                    'Transmitters' => [

                    ]
                ]
            ]
        ];
        \RBS\Selifa\Core::Initialize($coreOpts);

        $classes = get_declared_classes();
        $this->assertContains("RBS\\Selifa\\Configuration",$classes);
        $this->assertContains("RBS\\Selifa\\XM",$classes);
    }

    /**
     * @depends testCoreLoad
     */
    public function testExceptionManager()
    {
        try
        {
            throw new Exception('EXCEPTION TEST',500);
        }
        catch (Exception $x)
        {
            $result = \RBS\Selifa\XM::Instance()->Handle($x);

            $this->assertArrayHasKey('X',$result);
            $this->assertArrayHasKey('M',$result);
            $this->assertArrayHasKey('C',$result);
            $this->assertSame(9512,$result['C']);

            $this->assertArrayHasKey('Code',$result['X']);
            $this->assertSame(500,$result['X']['Code']);

            $this->assertArrayHasKey('Message',$result['X']);
            $this->assertSame('EXCEPTION TEST',$result['X']['Message']);
        }

        try
        {
            throw new SelifaException(0,'EXCEPTION TEST',['param1' => 'content']);
        }
        catch (Exception $x)
        {
            $result = \RBS\Selifa\XM::Instance()->Handle($x);

            $this->assertArrayHasKey('M',$result);
            $this->assertSame('EXCEPTION TEST',$result['M']);
        }

        \RBS\Selifa\XM::Instance()->SetOptions(['VerboseInternalException' => true]);
        try
        {
            throw new SelifaInternalException('EXCEPTION TEST');
        }
        catch (Exception $x)
        {
            $result = \RBS\Selifa\XM::Instance()->Handle($x);


            $this->assertArrayHasKey('C',$result);
            $this->assertSame(9015,$result['C']);

            $this->assertArrayHasKey('M',$result);
            $this->assertSame('EXCEPTION TEST',$result['M']);
        }
    }
}
?>