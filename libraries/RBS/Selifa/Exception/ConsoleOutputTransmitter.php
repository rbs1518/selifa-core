<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see readme.md.
 */

namespace RBS\Selifa\Exception;
use Exception;

/**
 * Class ConsoleOutputTransmitter
 *
 * @package RBS\Selifa\Exception
 * @copyright 2020-2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
class ConsoleOutputTransmitter implements IExceptionTransmitter
{
    /**
     * @param array $data
     * @param Exception $srcException
     */
    public function Send($data, $srcException)
    {
        $this->WriteToOutput($data,'');
    }

    /**
     * @param array $data
     * @param string $prefix
     */
    protected function WriteToOutput($data,$prefix="")
    {
        if ($prefix != '')
            echo "[".$data['C']."] ".$data['M']."\n";
        else
            echo "Exception: [".$data['C']."] ".$data['M']."\n";
        if ($data['X'] != null)
        {
            echo $prefix."\tMessage: ".$data['X']['Message']."\n";
            echo $prefix."\tFile: ".$data['X']['File']." (Line ".$data['X']['Line'].")\n";
        }

        if (isset($data['Previous']))
        {
            if ($data['Previous'] != null)
            {
                $prx = ($prefix."\t");
                echo "Previous ".$this->WriteToOutput($data['Previous'],$prx);
            }
        }
    }
}
?>