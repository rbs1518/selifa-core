<?php
namespace RBS\Selifa\Exception;
use Exception;
use RBS\Selifa\IComponent;
use RBS\Selifa\Traits\OptionableSingleton;

/**
 * Class Translator
 *
 * @package RBS\Selifa\Exception
 * @copyright 2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
class Translator implements IComponent, IExceptionTranslator
{
    use OptionableSingleton;

    /**
     * @var string
     */
    private $_Language = 'en';

    /**
     * @var string
     */
    private $_MessagePaths = '';

    /**
     * @var int
     */
    private $_MaxDigit = 6;

    /**
     * @param array $options
     */
    protected function _OnInit($options)
    {
        if (isset($options['DefaultLang']))
            $this->_Language = strtolower(trim($options['DefaultLang']));
        if (isset($options['BasePath']))
        {
            $bPath = trim($options['BasePath']);
            $this->_MessagePaths = (SELIFA_ROOT_PATH.DIRECTORY_SEPARATOR.$bPath);
        }
        else
            $this->_MessagePaths = (SELIFA_ROOT_PATH.DIRECTORY_SEPARATOR.'lang'.DIRECTORY_SEPARATOR);
        if (isset($options['MaxDigit']))
            $this->_MaxDigit = (int)$options['MaxDigit'];
    }

    /**
     * @param string $text
     * @param array $vars
     * @return string
     */
    protected function ReplaceVars($text,$vars)
    {
        $pattern = '@{(?<negate>[!]*?)!(?<key>[A-Za-z0-9_]+)}@';
        return preg_replace_callback($pattern,function($matches) use ($vars)
        {
            if (!isset($matches['key']))
                return '';
            $aKey = trim($matches['key']);
            if (!isset($vars[$aKey]))
                return '';
            return $vars[$aKey];
        },$text);
    }

    /**
     * @param string $lang
     * @return Translator
     */
    public function SetLanguage($lang)
    {
        $this->_Language = strtolower(trim($lang));
        return $this;
    }

    /**
     * @param int $eCode
     * @param array $data
     * @param Exception $exception
     * @return string
     */
    public function TranslateException($eCode, $data, $exception)
    {
        $tFile = ($this->_MessagePaths.$this->_Language.DIRECTORY_SEPARATOR.'error.php');
        if (!file_exists($tFile))
            return 'Unspecified error.';

        $messages = include($tFile);
        $sCode = (string)$eCode;
        if (isset($messages[$sCode]))
            return $this->ReplaceVars($messages[$sCode],$data);
        else
        {
            $rhCnt = 3;
            for ($i=$rhCnt;$i<=$this->_MaxDigit;$i++)
            {
                $fmt = '*'.substr($sCode,(-1)*$i);
                if (isset($messages[$fmt]))
                    return $this->ReplaceVars($messages[$fmt],$data);
            }
        }

        return 'Unspecified error.';
    }
}
?>