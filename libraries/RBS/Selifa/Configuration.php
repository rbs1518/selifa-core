<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see readme.md.
 */

namespace RBS\Selifa;
use Exception;
use ReflectionClass;

/**
 * Interface IConfigurationExtension
 *
 * @package RBS\Selifa
 * @copyright 2014-2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
interface IConfigurationExtension
{
    /**
     * @return string
     */
    public function GetConfigurationID();

    /**
     * @param string $configFileName
     * @param string $className
     * @return array
     */
    public function GetConfigurationData($configFileName,$className);
}

/**
 * Interface IConfigurationCache
 *
 * @package RBS\Selifa
 * @copyright 2014-2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
interface IConfigurationCache
{
    /**
     * Store data in cache specified by $key for a limited time specified by $expiryTime.
     *
     * @param string $key cache key.
     * @param mixed $value data to be stored.
     * @param int $expiryTime Time to live for data in seconds.
     */
    function Write($key,$value,$expiryTime);

    /**
     * Check whether specific data is exists or not.
     *
     * @param string $key cache key.
     * @return boolean
     */
    function Available($key);

    /**
     * Load data from cache specified by $key. Returns $default if data doest not exists.
     *
     * @param string $key cache key.
     * @param mixed|null $default Default value if cache's value is empty.
     * @return mixed|null
     */
    function Read($key,$default);

    /**
     * Remove data in cache specified by $Key
     *
     * @param string $key cache key
     * @return boolean
     */
    function Delete($key);
}

/**
 * Configuration manager used by selifa core to handle component's configuration.
 *
 * @package RBS\Selifa
 * @copyright 2014-2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
class Configuration
{
    /**
     * @var null|Configuration
     */
    private static $_Instance = null;

    /**
     * @var string
     */
    private $_DefaultConfigDir = '';

    /**
     * @var string
     */
    private $_AppConfigDir = '';

    /**
     * @var bool
     */
    private $_UseCache = false;

    /**
     * @var int
     */
    private $_EnvOption = LOAD_AND_PUT_ENV;

    /**
     * @var array
     */
    private $_InitialOptions = array();

    /**
     * @var null|IConfigurationExtension
     */
    private $_Extension = null;

    /**
     * @var null|IConfigurationCache
     */
    private $_Cache = null;

    /**
     * @var null|array
     */
    private $_AppEnvVars = array();

    /**
     * Register a configuration extension.
     * Configuration extension can be used to extend functionality of configuration manager or enables
     * custom configuration routine.
     *
     * This one is a static function.
     *
     * @param IConfigurationExtension $extension
     */
    public static function RegisterExtension(IConfigurationExtension $extension)
    {
        if (self::$_Instance != null)
            self::$_Instance->_Extension = $extension;
    }

    /**
     * @param string $defConfigDir
     * @param string $appConfigDir
     * @param bool $useCache
     * @param int $envOpt
     * @throws Exception
     */
    private function __construct($defConfigDir,$appConfigDir,$useCache,$envOpt)
    {
        $this->_DefaultConfigDir = $defConfigDir;
        $this->_AppConfigDir = $appConfigDir;
        $this->_UseCache = $useCache;
        $this->_EnvOption = $envOpt;

        if ($this->_EnvOption != IGNORE_ENV)
        {
            $envFile = (SELIFA_ROOT_PATH.'/.env');
            $envExFile = (SELIFA_ROOT_PATH.'/env-example');
            if (file_exists($envFile))
                $this->_AppEnvVars = $this->_ParseEnvFile($envFile);
            else if (file_exists($envExFile))
                $this->_AppEnvVars = $this->_ParseEnvFile($envExFile);
        }
    }

    /**
     * Parse simple env file.
     *
     * @param string $file
     * @return array
     * @throws Exception
     */
    private function _ParseEnvFile($file)
    {
        $rEnvContent = file_get_contents($file);
        $lines = explode("\n",$rEnvContent);

        $vars = array();
        foreach ($lines as $line)
        {
            $cLine = trim($line);
            if ($cLine == '')
                continue; //skip if empty line.

            if (substr($cLine,0,1) == '#')
                continue; //skip if commented line.

            $lPart = explode('=',$cLine,2);
            if (count($lPart) < 2)
                throw new \Exception('Invalid env file content. INVALID KEY-VALUE PAIR.');

            $aKey = strtoupper(trim($lPart[0]));

            $valueParts = explode('#',trim($lPart[1]));

            $aValue = trim($valueParts[0]);
            $fQuote = substr($aValue,0,1);
            $lQuote = substr($aValue,-1,1);
            if (($fQuote == "\"") && ($lQuote != "\""))
                throw new \Exception('Invalid env file content. UNTERMINATED QUOTE.');
            if (($fQuote == "'") && ($lQuote != "'"))
                throw new \Exception('Invalid env file content. UNTERMINATED QUOTE.');

            if (($fQuote == "\"") || ($fQuote == "'"))
            {
                $aValue = substr($aValue,1,strlen($aValue)-2);
                $vars[$aKey] = $aValue;
                if ($this->_EnvOption == LOAD_AND_PUT_ENV)
                    putenv($aKey.'="'.$aValue.'"');
            }
            else
            {
                $aCheck = strtolower($aValue);
                if ($aCheck == 'null')
                    $vars[$aKey] = null;
                else if ($aCheck == 'true')
                    $vars[$aKey] = true;
                else if ($aCheck == 'false')
                    $vars[$aKey] = false;
                else
                    $vars[$aKey] = $aValue;
                if ($this->_EnvOption == LOAD_AND_PUT_ENV)
                    putenv($aKey.'='.$aValue);
            }
        }

        return $vars;
    }

    /**
     * @param string $id
     * @param array $data
     */
    private function _CacheData($id,$data)
    {
        if (($this->_Cache != null) && ($this->_UseCache))
            $this->_Cache->Write('selifa.config.'.$id,$data,3600);
    }

    /**
     * @param string $id
     * @return mixed|null
     */
    private function _ReadCache($id)
    {
        if (($this->_Cache != null) && ($this->_UseCache))
        {
            $key = ('selifa.config.'.$id);
            if ($this->_Cache->Available($key))
                return $this->_Cache->Read($key,null);
        }
        return null;
    }

    /**
     * @param string $configFileName
     * @param string $className
     * @param array $temp
     */
    private function _LoadFromFiles($configFileName,$className,&$temp)
    {
        if (isset($this->_InitialOptions[$className]))
            $temp = $this->_InitialOptions[$className];

        $filePath = ($this->_DefaultConfigDir.$configFileName.'.php');
        $jsonFilePath = ($this->_DefaultConfigDir.$configFileName.'.json');
        if (file_exists($filePath))
            $temp = array_replace_recursive($temp,include($filePath));
        else if (file_exists($jsonFilePath))
            $temp = array_replace_recursive($temp,json_decode(file_get_contents($jsonFilePath),true));

        $aFilePath = ($this->_AppConfigDir.$configFileName.'.php');
        $aJSONFilePath = ($this->_AppConfigDir.$configFileName.'.json');
        if (file_exists($aFilePath))
            $temp = array_replace_recursive($temp,include($aFilePath));
        else if (file_exists($aJSONFilePath))
            $temp = array_replace_recursive($temp,json_decode(file_get_contents($aJSONFilePath),true));
    }

    /**
     * Set cache object using full-qualified class name.
     *
     * @param string $fqcn
     * @return Configuration
     * @throws Exception
     */
    public function UseCacheFromStringClassName($fqcn)
    {
        if (!class_exists($fqcn,true)) //Try to load the class.
            throw new Exception('Class '.$fqcn.' is not loaded or does not exists.');

        $rClass = new ReflectionClass($fqcn);
        if (!$rClass->implementsInterface('\RBS\Selifa\IConfigurationCache'))
            throw new Exception('Class '.$fqcn.' does not implement IConfigurationCache.');

        if ($rClass->implementsInterface('\RBS\Selifa\ISingletonGetter'))
        {
            $rMethod = $rClass->getMethod('Instance');
            $this->_Cache = $rMethod->invoke(null);
        }

        return $this;
    }

    /**
     * @param IConfigurationCache $cache
     * @return Configuration
     */
    public function UseCache(IConfigurationCache $cache)
    {
        $this->_Cache = $cache;
        return $this;
    }

    /**
     * @return null|IConfigurationCache
     */
    public function GetCacheObject()
    {
        return $this->_Cache;
    }

    /**
     * Register a configuration extension.
     * Configuration extension can be used to extend functionality of configuration manager or enables
     * custom configuration routine.
     *
     * @param IConfigurationExtension $extension
     * @return Configuration
     */
    public function Extend(IConfigurationExtension $extension)
    {
        $this->_Extension = $extension;
        return $this;
    }

    /**
     * Register a configuration extension.
     * Configuration extension can be used to extend functionality of configuration manager or enables
     * custom configuration routine.
     *
     * NB: Obsolete! Use Extend method instead.
     *
     * @param IConfigurationExtension $extension
     */
    public function SetExtension(IConfigurationExtension $extension)
    {
        $this->Extend($extension);
    }

    /**
     * Set initial option values for each class name.
     * Any value set with this method will be replaced with loaded configuration file if exists.
     *
     * Selifa core will try to load configuration assigned from this method on initialization.
     *
     * @param string $className
     * @param array $options
     * @return Configuration
     */
    public function SetInitialOptions($className,$options)
    {
        if ($className[0] == '\\')
            $aClassName = substr($className,1);
        else
            $aClassName = $className;
        $this->_InitialOptions[$aClassName] = $options;
        return $this;
    }

    /**
     * Load configuration for a component based on it's classname.
     * First, it will try to load a default configuration, then load from application's configuration path.
     * If an extension is registered, then it will try to call GetConfigurationData from extension object.
     *
     * 2020-02 Update: Added JSON file support. If no PHP file found, then it will try to look for similar name
     *                  but ended with .json extension.
     *
     * @param string $className The component class name.
     * @return array|mixed
     */
    public function LoadForComponent($className)
    {
        $configFileName = strtolower(str_replace("\\",'_',$className));

        if ($this->_Extension != null)
        {
            $configId = ($configFileName.'__'.$this->_Extension->GetConfigurationID());
            $temp = $this->_ReadCache($configId);
            if ($temp === null)
            {
                $temp = array();
                $this->_LoadFromFiles($configFileName,$className,$temp);
                $xOpts = $this->_Extension->GetConfigurationData($configFileName,$className);
                if ($xOpts != null)
                    $temp = array_replace_recursive($temp,$xOpts);
                $this->_CacheData($configId,$temp);
            }
            return $temp;
        }
        else
        {
            $temp = $this->_ReadCache($configFileName);
            if ($temp === null)
            {
                $temp = array();
                $this->_LoadFromFiles($configFileName,$className,$temp);
                $this->_CacheData($configFileName,$temp);
            }
            return $temp;
        }
    }

    /**
     * Initialize configuration manager object.
     *
     * @param string $defConfigDir
     * @param string $appConfigDir
     * @param bool $useCache
     * @param int $envOpt
     * @return null|Configuration
     * @throws Exception
     */
    public static function Initialize($defConfigDir='',$appConfigDir='',$useCache=false,$envOpt=LOAD_AND_PUT_ENV)
    {
        if (self::$_Instance == null)
            self::$_Instance = new Configuration($defConfigDir,$appConfigDir,$useCache,$envOpt);
        return self::$_Instance;
    }

    /**
     * @return null|Configuration
     */
    public static function Instance()
    {
       return self::$_Instance;
    }

    /**
     * @param string|array $keyName
     * @param mixed|null $defaultValue
     * @param bool $asBoolean
     * @return mixed|null
     */
    public static function Env($keyName,$defaultValue=null,$asBoolean=false)
    {
        if (is_array($keyName))
        {
            foreach ($keyName as $key)
            {
                $value = getenv($key,true);
                if ($value === false)
                    continue;

                if ($asBoolean)
                    return (strtolower(trim($value)) == 'true');
                else
                {
                    if (($value === "''") || ($value === '""'))
                        return '';
                    else
                        return $value;
                }
            }
            return $defaultValue;
        }
        else
        {
            $value = getenv($keyName,true)?:(getenv($keyName)?:$defaultValue);
            if ($asBoolean)
                return (strtolower(trim($value)) == 'true');
            else
            {
                if (($value === "''") || ($value === '""'))
                    return '';
                else
                    return $value;
            }
        }
    }

    /**
     * @param string $keyName
     * @param mixed|null $defaultValue
     * @param bool $asBoolean
     * @return mixed|null
     */
    public static function AppEnv($keyName,$defaultValue=null,$asBoolean=false)
    {
        $i = self::$_Instance;
        $value = (isset($i->_AppEnvVars[$keyName])?$i->_AppEnvVars[$keyName]:$defaultValue);
        if (($value === "''") || ($value === '""'))
            return '';
        else
            return $value;
    }
}
?>