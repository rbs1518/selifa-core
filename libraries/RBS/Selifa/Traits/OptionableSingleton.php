<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see readme.md.
 */

namespace RBS\Selifa\Traits;

/**
 * trait OptionableSingleton
 *
 * @package RBS\Selifa\Traits
 * @copyright 2020-2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
trait OptionableSingleton
{
    /**
     * @var null|static
     */
    protected static $_Instance = null;

    /**
     * @var null|array
     */
    protected $_Options = null;

    /**
     * @param array $options
     * @return static
     */
    public static function Initialize($options)
    {
        if (self::$_Instance == null)
            self::$_Instance = new static($options);
        return self::$_Instance;
    }

    /**
     * @return static
     */
    public static function Instance()
    {
        return self::$_Instance;
    }

    /**
     * @param array $options
     */
    final private function __construct($options)
    {
        $this->_Options = $options;
        $this->_OnInit($options);
    }

    /**
     * @param array $options
     */
    protected function _OnInit($options) { }
}
?>