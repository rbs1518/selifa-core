<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see readme.md.
 */

namespace
{
    define('SELIFA_EXCEPTION_SYSTEM_START_CODE',9000);
    define('SELIFA_EXCEPTION_START_CODE', 10000);
    define('SELIFA_EXCEPTION_NULL', SELIFA_EXCEPTION_SYSTEM_START_CODE + 11);
    define('SELIFA_EXCEPTION_SYSTEM', SELIFA_EXCEPTION_SYSTEM_START_CODE + 12);
    define('SELIFA_EXCEPTION_UNDEFINED', SELIFA_EXCEPTION_SYSTEM_START_CODE + 13);
    define('SELIFA_EXCEPTION_CODE_OUT_OF_RANGE', SELIFA_EXCEPTION_SYSTEM_START_CODE + 14);
    define('SELIFA_EXCEPTION_INTERNAL', SELIFA_EXCEPTION_SYSTEM_START_CODE + 15);

    define('SELIFA_EXCEPTION_NON_ZERO_IDENTIFIER', SELIFA_EXCEPTION_START_CODE + 11);
    define('SELIFA_EXCEPTION_PARAMETER_IS_MISSING', SELIFA_EXCEPTION_START_CODE + 12);
    define('SELIFA_EXCEPTION_VALUE_IS_MISSING', SELIFA_EXCEPTION_START_CODE + 13);

    /**
     * Class SelifaInternalException
     * @copyright RBS 2014
     * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
     */
    class SelifaInternalException extends Exception
    {
        /**
         * @param string $message
         * @param Exception|null $previousException
         */
        public function __construct($message = '', $previousException = null)
        {
            $eCode = SELIFA_EXCEPTION_INTERNAL;
            parent::__construct($message, $eCode, $previousException);
        }
    }

    /**
     * Class SelifaException
     * @copyright RBS 2014
     * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
     */
    class SelifaException extends Exception
    {
        /**
         * @var array
         */
        public $ArrayData;

        /**
         * @param int $eCode
         * @param string $message
         * @param array $arrayData
         * @param Exception|null $previousException
         */
        public function __construct($eCode, $message = '', $arrayData = array(), $previousException = null)
        {
            parent::__construct($message, $eCode, $previousException);
            $this->ArrayData = $arrayData;
        }

        /**
         * @param array $aData
         */
        public function AppendData($aData)
        {
            $this->ArrayData = array_merge_recursive($this->ArrayData,$aData);
        }
    }
}

namespace RBS\Selifa
{
    use RBS\Selifa\Traits\OptionableSingleton;
    use Exception;
    use SelifaException;
    use SelifaInternalException;
    use RBS\Selifa\Exception\IExceptionTransmitter;
    use RBS\Selifa\Exception\IExceptionTranslator;
    use ReflectionClass;
    use ReflectionMethod;

    /**
     * Class XM
     *
     * Integrated exception handle and management for Selifa Framework.
     *
     * @package RBS\Selifa
     * @copyright 2014-2021
     * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
     */
    class XM implements IComponent
    {
        use OptionableSingleton;

        /**
         * @var bool
         */
        private $_ShowExceptionTrace = true;

        /**
         * @var bool
         */
        private $_VerboseInternalException = false;

        /**
         * @var bool
         */
        private $_VerboseSystemException = false;

        /**
         * @var bool
         */
        private $_HandleDefaultException = true;

        /**
         * @var string
         */
        private $_TranslatorClass = '';

        /**
         * @var string[]
         */
        private $_TransmitterClasses = '';

        /**
         * @var bool
         */
        private $_TraceExceptionTree = false;

        /**
         * @param array $options
         */
        protected function _OnInit($options)
        {
            $this->SetOptions($options);
            $this->EnableExceptionHandler();
        }

        /**
         * @param array $data
         * @param Exception $srcException
         */
        protected function CreateTransmittersAndSend($data,$srcException)
        {
            foreach ($this->_TransmitterClasses as $className)
            {
                $trObj = new $className();
                if ($trObj instanceof IExceptionTransmitter)
                    $trObj->Send($data,$srcException);
            }
        }

        /**
         * @param int $eCode
         * @param array $data
         * @param Exception $exception
         * @return string
         */
        protected function Translate($eCode,$data,$exception)
        {
            if ($this->_TranslatorClass == '')
                return (string)$eCode;
            try
            {
                $className = $this->_TranslatorClass;
                $r = new ReflectionClass($className);

                $xMethod = null;
                foreach ($r->getMethods(ReflectionMethod::IS_STATIC) as $m)
                {
                    if ($m->getName() == 'Instance')
                        $xMethod = $m;
                }
                if ($xMethod === null)
                    $trlObject = new $className();
                else
                    $trlObject = $xMethod->invoke(null);
                if ($trlObject instanceof IExceptionTranslator)
                    return $trlObject->TranslateException($eCode,$data,$exception);
                else
                    return (string)$eCode;
            }
            catch (Exception $x)
            {
                return ('TRANSLATE EXCEPTION: '.$x->getMessage());
            }
        }

        /**
         * @param array $options
         */
        public function SetOptions($options)
        {
            if (isset($options['EnableTrace']))
                $this->_ShowExceptionTrace = (bool)$options['EnableTrace'];
            if (isset($options['VerboseInternalException']))
                $this->_VerboseInternalException = (bool)$options['VerboseInternalException'];
            if (isset($options['VerboseSystemException']))
                $this->_VerboseSystemException = (bool)$options['VerboseSystemException'];
            if (isset($options['TraceExceptionTree']))
                $this->_TraceExceptionTree = (bool)$options['TraceExceptionTree'];
            if (isset($options['HandleDefaultException']))
                $this->_HandleDefaultException = (bool)$options['HandleDefaultException'];
            if (isset($options['TranslatorClass']))
                $this->_TranslatorClass = trim($options['TranslatorClass']);

            if (isset($options['Transmitters']))
            {
                if (is_string($options['Transmitters']))
                    $this->_TransmitterClasses = [$options['Transmitters']];
                else if (is_array($options['Transmitters']))
                    $this->_TransmitterClasses = $options['Transmitters'];
            }
        }

        /**
         * @param Exception|null $x
         * @return array|null
         */
        public function ExceptionToArray($x)
        {
            if ($x != null)
            {
                $data = [
                    'Code' => $x->getCode(),
                    'Message' => $x->getMessage(),
                    'Line' => $x->getLine(),
                    'File' => $x->getFile()
                ];

                if ($this->_ShowExceptionTrace)
                    $data['Trace'] = $x->getTrace();
                if ($this->_TraceExceptionTree)
                    $data['Previous'] = $this->ExceptionToArray($x->getPrevious());
                return $data;
            }
            return null;
        }

        /**
         * Set the class that will be used for translating error code into human-readable localized message.
         * Refer to IExceptionTranslator interface for usage information.
         *
         * @param string $className
         * @return XM
         */
        public function SetTranslator($className)
        {
            $this->_TranslatorClass = trim($className);
            return $this;
        }

        /**
         * Add exception transmitter class to existing stack.
         *
         * @param string $transmitterClass
         * @return XM
         */
        public function AddTransmitter($transmitterClass)
        {
            $this->_TransmitterClasses = $transmitterClass;
            return $this;
        }

        /**
         * @param Exception|SelifaException|SelifaInternalException $x
         * @return array
         */
        public function Handle($x)
        {
            $result = ['X' => null, 'M' => '', 'C' => SELIFA_EXCEPTION_NULL];
            if ($x instanceof SelifaException)
            {
                $eCode = (int)$x->getCode();
                $result['C'] = $eCode;
                if (($eCode > 0) && ($this->_TranslatorClass != ''))
                    $result['M'] = $this->Translate($eCode,$x->ArrayData,$x);
                else
                    $result['M'] = $x->getMessage();
            }
            else if ($x instanceof SelifaInternalException)
            {
                $result['C'] = (int)$x->getCode();
                if ($this->_VerboseInternalException)
                    $result['M'] = $x->getMessage();
                else
                    $result['M'] = 'Internal exception occurs.';
            }
            else
            {
                $code = (int)$x->getCode();
                if ($code > 0)
                    $result['C'] = $code;
                else
                    $result['C'] = 0;
                if ($this->_TranslatorClass != '')
                    $result['M'] = $this->Translate($result['C'],[],$x);
                else
                {
                    if ($this->_VerboseSystemException)
                    {
                        $s = 'Exception at line ' . $x->getLine() . ' at file ' . $x->getFile() . ', ' . $x->getMessage();
                        $result['M'] = $s;
                    }
                    else
                        $result['M'] = 'System exception occurs.';
                }
            }
            $result['X'] = $this->ExceptionToArray($x);
            $this->CreateTransmittersAndSend($result,$x);
            return $result;
        }
        
        /**
         * @param mixed|null $var
         */
        public static function DumpToErrorLog($var)
        {
            ob_start();
            var_dump($var);
            $s = ob_get_contents();
            ob_end_clean();
            error_log('DUMP: '.$s,0);
        }

        /**
         * Handle uncaught exception, and redirect it to XM::Handle method.
         */
        public function EnableExceptionHandler()
        {
            if ($this->_HandleDefaultException)
            {
                set_exception_handler(function($x)
                {
                    self::$_Instance->Handle($x);
                });
            }
        }
    }
}
?>